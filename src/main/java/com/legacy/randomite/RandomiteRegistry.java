package com.legacy.randomite;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

@EventBusSubscriber(modid = RandomiteMod.MODID, bus = Bus.MOD)
public class RandomiteRegistry
{
	public static final Block RANDOMITE_ORE = new Block(Block.Properties.copy(Blocks.DIAMOND_ORE));
	public static final Block DEEPSLATE_RANDOMITE_ORE = new Block(Block.Properties.copy(Blocks.DEEPSLATE_DIAMOND_ORE));

	@SubscribeEvent
	public static void onRegisterItems(Register<Item> event)
	{
		register(event.getRegistry(), "randomite_ore", new BlockItem(RANDOMITE_ORE, new Item.Properties().tab(CreativeModeTab.TAB_BUILDING_BLOCKS)));
		register(event.getRegistry(), "deepslate_randomite_ore", new BlockItem(DEEPSLATE_RANDOMITE_ORE, new Item.Properties().tab(CreativeModeTab.TAB_BUILDING_BLOCKS)));
	}

	@SubscribeEvent
	public static void onRegisterBlocks(Register<Block> event)
	{
		register(event.getRegistry(), "randomite_ore", RANDOMITE_ORE);
		register(event.getRegistry(), "deepslate_randomite_ore", DEEPSLATE_RANDOMITE_ORE);
	}

	public static <T extends IForgeRegistryEntry<T>> void register(IForgeRegistry<T> registry, String name, T object)
	{
		object.setRegistryName(RandomiteMod.locate(name));
		registry.register(object);
	}
}