package com.legacy.randomite;

import java.util.List;

import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.features.OreFeatures;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.OreConfiguration;
import net.minecraft.world.level.levelgen.placement.BiomeFilter;
import net.minecraft.world.level.levelgen.placement.CountPlacement;
import net.minecraft.world.level.levelgen.placement.HeightRangePlacement;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.fml.common.Mod;

@Mod(RandomiteMod.MODID)
public class RandomiteMod
{
	public static final String NAME = "Randomite Classic";
	public static final String MODID = "randomite";

	public RandomiteMod()
	{
		RandomiteFeatures.init();

		MinecraftForge.EVENT_BUS.addListener((BiomeLoadingEvent event) ->
		{
			if ((event.getName().getNamespace().equalsIgnoreCase("minecraft") || event.getName().getNamespace().equalsIgnoreCase("biomesoplenty") || event.getName().getNamespace().equalsIgnoreCase("byg")) && event.getCategory() != Biome.BiomeCategory.NETHER && event.getCategory() != Biome.BiomeCategory.THEEND && event.getCategory() != Biome.BiomeCategory.NONE)
				event.getGeneration().addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RandomiteFeatures.RANDOMITE_ORE);
		});
	}

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(RandomiteMod.MODID, name);
	}

	public static String find(String name)
	{
		return RandomiteMod.MODID + ":" + name;
	}

	public static class RandomiteFeatures
	{
		public static final List<OreConfiguration.TargetBlockState> RANDOMITE_ORE_TARGET_LIST = List.of(OreConfiguration.target(OreFeatures.STONE_ORE_REPLACEABLES, RandomiteRegistry.RANDOMITE_ORE.defaultBlockState()), OreConfiguration.target(OreFeatures.DEEPSLATE_ORE_REPLACEABLES, RandomiteRegistry.DEEPSLATE_RANDOMITE_ORE.defaultBlockState()));
		public static final ConfiguredFeature<?, ?> CONFIGURED_RANDOMITE_ORE = FeatureUtils.register(find("randomite_ore"), Feature.ORE.configured(new OreConfiguration(RANDOMITE_ORE_TARGET_LIST, 4)));

		public static final PlacedFeature RANDOMITE_ORE = PlacementUtils.register(find("randomite_ore"), CONFIGURED_RANDOMITE_ORE.placed(List.of(CountPlacement.of(2), InSquarePlacement.spread(), HeightRangePlacement.triangle(VerticalAnchor.absolute(-32), VerticalAnchor.absolute(32)), BiomeFilter.biome())));

		public static void init()
		{
		}
	}
}